<?php
setlocale(LC_ALL, 'fr_FR');
date_default_timezone_set('Europe/Paris');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,900" rel="stylesheet">
    <link rel="stylesheet" href="https://code.sportysky.com/sportysky/1.0/api.sportysky.min.css"/>
    <link rel="stylesheet" href="map.css"/>
    <title>SportySKY Map Demo</title>
</head>
<body>
<div class="container">
    <h1><?php print $_GET['isocode']; ?></h1>
    <div class="sportysky">
        <p>Météo propulsée par </p><img src="img/logo-sportysky-sans-fond-8.png" width="150" alt="Météo propulsée par Sportysky"/>
    </div>

    <div class="pictos">
        <img src="https://code.sportysky.com/sportysky/1.0/img/pollution.svg" alt="Pollution" alt="CO2" id="show-aq" class="button">
        <img src="https://code.sportysky.com/sportysky/1.0/img/vent.svg" alt="Vent" id="show-wind" class="button"/>
        <img src="https://code.sportysky.com/sportysky/1.0/img/temps-sensible.svg" alt="Temps sensible" id="show-weather" class="button"/>
    </div>

    <div id="no_aq">Pas de qualité de l'air pour la date donnée</div>

    <!-- MAP -->
    <div id="map"></div>
    <!-- end MAP -->

    <div class="timeline">
        <div class="timeline_days">
            <div class="days">
                <?php for ($i = 0; $i <= 7; $i++): ?>
                    <?php
                    $time = new DateTime();
                    $time->setTimestamp(strtotime('+'.$i.' day'));
                    $time->setTime(0, 0);;
                    ?>
                    <div class="bloc_day button <?php echo ($i == 0) ? 'activ' : ''; ?>" id="day_<?php print $i; ?>"
                         data-time="<?php print $time->getTimestamp() * 1000; ?>">
                        <p>
                            <?php if ($i == 0): ?>
                                Aujourd'hui
                            <?php elseif ($i == 1): ?>
                                Demain
                            <?php else: ?>
                                <?php print $time->format('l d/m'); ?>
                            <?php endif; ?>
                        </p>
                    </div>
                <?php endfor; ?>
            </div> <!-- end days -->
        </div> <!-- end timeline_days-->

        <div class="heures" id="heures_list">
            <ul>
                <li class="bloc_heures button <?= ((date('G') >= 0) && (date('G') < 10)) ? 'activ' : ''; ?>" id="date_morning">Matin</li>
                <li class="bloc_heures button <?= ((date('G') >= 10) && (date('G') < 16)) ? 'activ' : ''; ?>" id="date_midday">Midi</li>
                <li class="bloc_heures button <?= ((date('G') >= 16) && (date('G') < 22)) ? 'activ' : ''; ?>" id="date_evening">Soirée</li>
                <li class="bloc_heures button <?= ((date('G') >= 22) || (date('G') < 0)) ? 'activ' : ''; ?>" id="date_night">Nuit</li>
            </ul>
        </div> <!-- end heures-->

        <div class="chevrons">
            <div class="previous button" id="previous_date">
                <img src="https://code.sportysky.com/sportysky/1.0/img/left-chevron.svg" alt="">
            </div>
            <div class="next button" id="next_date">
                <img src="https://code.sportysky.com/sportysky/1.0/img/right-chevron.svg" alt="">
            </div>
        </div> <!-- end chevrons-->

    </div> <!-- end timeline-->
    <div id="text_follow"></div>
</div> <!-- end container-->

<script type="text/javascript">
    var currentType = 'department';
    var currentIso = '<?php print $_GET['isocode']; ?>';
</script>
<script src="https://code.sportysky.com/sportysky/1.0/api.sportysky.min.js"></script>
<script src="script_demo_map.js"></script>
</body>
</html>