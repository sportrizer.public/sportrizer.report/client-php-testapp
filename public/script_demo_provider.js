window.addEventListener("DOMContentLoaded", function () {

    var slider, sizeChilds, currentSlideIndex = 0, widthSlideItem = 0;
    var nextSlide = function () {
        if (currentSlideIndex >= (sizeChilds - 1)) {
            return;
        }
        currentSlideIndex++;
        var currentItem = document.getElementsByClassName('slide active')[0];
        var style = getComputedStyle(currentItem);
        var width = currentItem.offsetWidth;
        width += parseInt(style.marginLeft) + parseInt(style.marginRight);
        slider.style.marginLeft = (-1 * (currentSlideIndex * width)) + "px";
        currentItem.classList.remove('active');
        slider.children[currentSlideIndex].classList.add('active');
    };

    var prevSlide = function () {
        if (currentSlideIndex == 0) {
            return;
        }
        currentSlideIndex--;
        var currentItem = document.getElementsByClassName('slide active')[0];
        var style = getComputedStyle(currentItem);
        var width = currentItem.offsetWidth;
        width += parseInt(style.marginLeft) + parseInt(style.marginRight);
        slider.style.marginLeft = (currentSlideIndex * width) + "px";
        currentItem.classList.remove('active');
        slider.children[currentSlideIndex].classList.add('active');
    };

    var initSlider = function (elmSlider, prevBtn, nextBtn) {
        slider = elmSlider;

        sizeChilds = slider.children.length;
        slider.children[currentSlideIndex].classList.add('active');
        widthSlideItem = slider.children[currentSlideIndex].offsetWidth;

        prevBtn.addEventListener('click', prevSlide);
        nextBtn.addEventListener('click', nextSlide);

    };

    initSlider(document.getElementById('items'), document.getElementById('prev'), document.getElementById('next'));
});