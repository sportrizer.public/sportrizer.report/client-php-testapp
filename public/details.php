<?php
declare(strict_types=1);
require __DIR__.'/SportyskyTools.class.php';

setlocale(LC_TIME, 'fr');
date_default_timezone_set('Europe/Paris');

use Sportrizer\Sportysky\ApiClient;
use Sportrizer\Sportysky\Authenticator;
use GuzzleHttp\HandlerStack;
use Kevinrob\GuzzleCache\CacheMiddleware;
use Kevinrob\GuzzleCache\Strategy\PublicCacheStrategy;
use Doctrine\Common\Cache\FilesystemCache;
use Kevinrob\GuzzleCache\Storage\DoctrineCacheStorage;
use SportyskyTools\SportyskyTools;

use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Middleware;

require '../vendor/autoload.php';

$spot_uuid = $_GET['spot'];

$authenticator = new Authenticator(getenv('SPORTYSKY_CLIENT_ID'), getenv('SPORTYSKY_CLIENT_SECRET'));

$cacheHandler = HandlerStack::create();
$cacheHandler->push(
    new CacheMiddleware(
        new PublicCacheStrategy(
            new DoctrineCacheStorage(
                new FilesystemCache('/tmp/demomap')
            )
        )
    )
);
$apiClient = new ApiClient($authenticator->getToken(), $cacheHandler);

$first_date = (new \DateTime())->setTime(0, 0, 0);
$end_date   = (new \DateTime())->setTime(23, 59, 59);
$response   = $apiClient->getSpotForecastResponse($spot_uuid, $first_date, $end_date);
$data_spot  = json_decode($response->getBody()->getContents(), true);


$morning = (new \DateTime())->setTime(8, 0, 0)->setTimezone(new DateTimeZone('Europe/Paris'));
$midday  = (new \DateTime())->setTime(13, 0, 0)->setTimezone(new DateTimeZone('Europe/Paris'));
$evening = (new \DateTime())->setTime(17, 0, 0)->setTimezone(new DateTimeZone('Europe/Paris'));
$night   = (new \DateTime())->setTime(23, 0, 0)->setTimezone(new DateTimeZone('Europe/Paris'));

$morning_feed = [];
$midday_feed  = [];
$evening_feed = [];
$night_feed   = [];

foreach ($data_spot['spots'][0]['feeds'] as $feed) {
    $date_feed = new \DateTime($feed['date']);
    switch ($date_feed) {
        case $morning:
            $morning_feed = $feed;
            break;
        case $midday:
            $midday_feed = $feed;
            break;
        case $evening:
            $evening_feed = $feed;
            break;
        case $night:
            $night_feed = $feed;
            break;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,900" rel="stylesheet">
    <link rel="stylesheet" href="provider.css"/>
    <title>SportySKY Map Demo</title>
</head>
<body>
<div class="container">
    <h1><?php print $data_spot['spots'][0]['title']; ?></h1>
    <div class="sportysky">
        <p>Météo propulsée par </p><img src="img/logo-sportysky-sans-fond-8.png" width="150" alt="Météo propulsée par Sportysky"/>
    </div>

    <!-- Block temps sensible -->
    <div class="encart">
        <div class="block">
            <h4>Matin</h4>
            <div class="weatherBlock">
                <img src="<?php print $morning_feed['icon']; ?>"
                     alt="<?php print (new SportyskyTools())->weatherConditionsToTxt($morning_feed['weatherIconIndex']); ?>">
                <?php print $morning_feed['airTemperature']; ?>°
            </div>
            <p><?php print (new SportyskyTools())->weatherConditionsToTxt($morning_feed['weatherIconIndex']); ?></p>
        </div>
        <div class="block">
            <h4>après-midi</h4>
            <div class="weatherBlock">
                <img src="<?php print $midday_feed['icon']; ?>"
                     alt="<?php print (new SportyskyTools())->weatherConditionsToTxt($midday_feed['weatherIconIndex']); ?>">
                <?php print $midday_feed['airTemperature']; ?>°
            </div>
            <p><?php print (new SportyskyTools())->weatherConditionsToTxt($midday_feed['weatherIconIndex']); ?></p>
        </div>
        <div class="block">
            <h4>soirée</h4>
            <div class="weatherBlock">
                <img src="<?php print $evening_feed['icon']; ?>"
                     alt="<?php print (new SportyskyTools())->weatherConditionsToTxt($evening_feed['weatherIconIndex']); ?>">
                <?php print $evening_feed['airTemperature']; ?>°
            </div>
            <p><?php print (new SportyskyTools())->weatherConditionsToTxt($evening_feed['weatherIconIndex']); ?></p>
        </div>
        <div class="block blocNuit">
            <h4>nuit</h4>
            <div class="weatherBlock">
                <img src="<?php print $night_feed['icon']; ?>"
                     alt="<?php print (new SportyskyTools())->weatherConditionsToTxt($night_feed['weatherIconIndex']); ?>">
                <?php print $night_feed['airTemperature']; ?>°
            </div>
            <p><?php print (new SportyskyTools())->weatherConditionsToTxt($night_feed['weatherIconIndex']); ?></p>
        </div>
    </div>
    <br>

    <!-- Block direction vent -->
    <div class="encartWind">
        <div class="windBlock">
            <h4>Vent <?php print (new SportyskyTools())->windDirectionToTxt($morning_feed['windDirectionString']); ?></h4>
            <div class="wind">
                <img src="<?php print $morning_feed['windIcon']; ?>" alt="Vent"
                     class="<?php print strtolower($morning_feed['windDirectionString']); ?>"> <br>
                <?php print ceil($morning_feed['windSpeedKMH']); ?> km/h <br>
                <?php print ($morning_feed['windGustKMH'] >= (ceil($morning_feed['windSpeedKMH']) + 18)) ? 'Rafales à '.ceil(
                        $morning_feed['windGustKMH']
                    ).' km/h' : ''; ?>

            </div>
            <div class="air">
                <img src="<?php print $morning_feed['airQualityIcon']; ?>"
                     alt="<?php print (new SportyskyTools())->aqIndexToTxt($morning_feed['airQualityIconIndex']); ?>">
                Niveau de pollution <br>
                <?php print (new SportyskyTools())->aqIndexToTxt($morning_feed['airQualityIconIndex']); ?>
            </div>
        </div>
        <div class="windBlock">
            <h4>Vent <?php print (new SportyskyTools())->windDirectionToTxt($midday_feed['windDirectionString']); ?></h4>
            <div class="wind">
                <img src="<?php print $midday_feed['windIcon']; ?>" alt="Vent"
                     class="<?php print strtolower($midday_feed['windDirectionString']); ?>"> <br>
                <?php print ceil($midday_feed['windSpeedKMH']); ?> km/h <br>
                <?php print ($midday_feed['windGustKMH'] >= (ceil($midday_feed['windSpeedKMH']) + 18)) ? 'Rafales à '.ceil(
                        $midday_feed['windGustKMH']
                    ).' km/h' : ''; ?>
            </div>
            <div class="air">
                <img src="<?php print $midday_feed['airQualityIcon']; ?>"
                     alt="<?php print (new SportyskyTools())->aqIndexToTxt($midday_feed['airQualityIconIndex']); ?>">
                Niveau de pollution <br>
                <?php print (new SportyskyTools())->aqIndexToTxt($midday_feed['airQualityIconIndex']); ?>
            </div>
        </div>
        <div class="windBlock">
            <h4>Vent <?php print (new SportyskyTools())->windDirectionToTxt($evening_feed['windDirectionString']); ?></h4>
            <div class="wind">
                <img src="<?php print $evening_feed['windIcon']; ?>" alt="Vent"
                     class="<?php print strtolower($evening_feed['windDirectionString']); ?>"> <br>
                <?php print ceil($evening_feed['windSpeedKMH']); ?> km/h <br>
                <?php print ($evening_feed['windGustKMH'] >= (ceil($evening_feed['windSpeedKMH']) + 18)) ? 'Rafales à '.ceil(
                        $evening_feed['windGustKMH']
                    ).' km/h' : ''; ?>
            </div>
            <div class="air">
                <img src="<?php print $evening_feed['airQualityIcon']; ?>"
                     alt="<?php print (new SportyskyTools())->aqIndexToTxt($evening_feed['airQualityIconIndex']); ?>">
                Niveau de pollution <br>
                <?php print (new SportyskyTools())->aqIndexToTxt($evening_feed['airQualityIconIndex']); ?>
            </div>
        </div>
        <div class="windBlock nightWind">
            <h4>Vent <?php print (new SportyskyTools())->windDirectionToTxt($night_feed['windDirectionString']); ?></h4>
            <div class="wind">
                <img src="<?php print $night_feed['windIcon']; ?>" alt="Vent" class="<?php print strtolower($night_feed['windDirectionString']); ?>">
                <br>
                <?php print ceil($night_feed['windSpeedKMH']); ?> km/h <br>
                <?php print ($night_feed['windGustKMH'] >= (ceil($night_feed['windSpeedKMH']) + 18)) ? 'Rafales à '.ceil($night_feed['windGustKMH'])
                    .' km/h' : ''; ?>
            </div>
            <div class="air">
                <img src="<?php print $night_feed['airQualityIcon']; ?>"
                     alt="<?php print (new SportyskyTools())->aqIndexToTxt($night_feed['airQualityIconIndex']); ?>">
                Niveau de pollution <br>
                <?php print (new SportyskyTools())->aqIndexToTxt($night_feed['airQualityIconIndex']); ?>
            </div>
        </div>
    </div>

    <!-- Param mer -->
    <!--<div class="sea">
        <p>Pleine mer : 11h50 coeff. 54</p>
        <p>Basse mer : 17h15</p>
    </div>-->

    <h2>Météo des 8 prochains jours à <?php print $data_spot['spots'][0]['title']; ?></h2>

    <!-- Méteo jours -->
    <div class="slider" id="slider">
        <div class="wrapper">
            <div id="items" class="items">

                <div class="slide">
                    <?php for ($i = 1; $i <= 4; $i++): ?>
                        <?php
                        $first_date = (new \DateTime())->add(new DateInterval('P'.$i.'D'))->setTime(0, 0, 0);
                        $end_date   = (new \DateTime())->add(new DateInterval('P'.$i.'D'))->setTime(23, 59, 59);
                        $response   = $apiClient->getSpotForecastResponse($spot_uuid, $first_date, $end_date);
                        $data_spot  = json_decode($response->getBody()->getContents(), true);
                        $min_temp   = 99;
                        $max_temp   = 00;
                        $icon       = [];
                        foreach ($data_spot['spots'][0]['feeds'] as $feed) {
                            $dateFeed = new \DateTime($feed['date']);
                            if ($feed['airTemperature'] < $min_temp) {
                                $min_temp = $feed['airTemperature'];
                            }
                            if ($feed['airTemperature'] > $max_temp) {
                                $max_temp = $feed['airTemperature'];
                            }
                            if ($dateFeed->format('H') == '12') {
                                $icon = ['src' => $feed['icon'], 'alt' => (new SportyskyTools())->weatherConditionsToTxt($feed['weatherIconIndex'])];
                            }
                        }
                        ?>
                        <div class="days">
                            <p><?php print strftime('%A', $first_date->getTimestamp()); ?></p>
                            <p><?php print strftime('%d', $first_date->getTimestamp()); ?></p>
                            <div class="picto">
                                <img src="<?php print $icon['src']; ?>"
                                     alt="<?php print $icon['alt']; ?>">
                            </div>
                            <div class=" temp">
                                <p><?php print $min_temp; ?>°</p>
                                <hr>
                                <p><?php print $max_temp; ?>°</p>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
                <div class="slide">
                    <?php for ($i = 5; $i <= 8; $i++): ?>
                        <?php
                        $first_date = (new \DateTime())->add(new DateInterval('P'.$i.'D'))->setTime(0, 0, 0);
                        $end_date   = (new \DateTime())->add(new DateInterval('P'.$i.'D'))->setTime(23, 59, 59);
                        $response   = $apiClient->getSpotForecastResponse($spot_uuid, $first_date, $end_date);
                        $data_spot  = json_decode($response->getBody()->getContents(), true);
                        $min_temp   = 99;
                        $max_temp   = 00;
                        $icon       = [];
                        foreach ($data_spot['spots'][0]['feeds'] as $feed) {
                            $dateFeed = new \DateTime($feed['date']);
                            if ($feed['airTemperature'] < $min_temp) {
                                $min_temp = $feed['airTemperature'];
                            }
                            if ($feed['airTemperature'] > $max_temp) {
                                $max_temp = $feed['airTemperature'];
                            }
                            if ($dateFeed->format('H') == '12') {
                                $icon = ['src' => $feed['icon'], 'alt' => (new SportyskyTools())->weatherConditionsToTxt($feed['weatherIconIndex'])];
                            }
                        }
                        ?>
                        <div class="days">
                            <p><?php print strftime('%A', $first_date->getTimestamp()); ?></p>
                            <p><?php print strftime('%d', $first_date->getTimestamp()); ?></p>
                            <div class="picto">
                                <img src="<?php print $icon['src']; ?>"
                                     alt="<?php print $icon['alt']; ?>">
                            </div>
                            <div class=" temp">
                                <p><?php print $min_temp; ?>°</p>
                                <hr>
                                <p><?php print $max_temp; ?>°</p>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
        <a id="prev" class="control prev"></a>
        <a id="next" class="control next"></a>
    </div>
</div>

<script src="script_demo_provider.js"></script>
</body>
</html>