<?php

namespace SportyskyTools;


class SportyskyTools
{
    const PATH_TO_LANG = __DIR__.'/lang';

    /**
     * @param string $direction
     * @param string $lang
     *
     * @return mixed|string
     */
    public function windDirectionToTxt($direction = '', $lang = 'fr')
    {
        $windText = [];
        include(self::PATH_TO_LANG.'/'.$lang.'.lang.php');
        if (isset($windText['WIND_'.$direction])) {
            return $windText['WIND_'.$direction];
        }

        return '';
    }

    public function weatherConditionsToTxt($weatherIndex = 0, $lang = 'fr')
    {
        $weatherIndex = (int) $weatherIndex;
        $forecastText = [];
        include(self::PATH_TO_LANG.'/'.$lang.'.lang.php');
        if (isset($forecastText['WEATHER_'.$weatherIndex])) {
            return $forecastText['WEATHER_'.$weatherIndex];
        }

        return '';
    }

    public function aqIndexToTxt($aqIndex = 0, $lang = 'fr')
    {
        $aqIndex = (int) $aqIndex;
        $aqText  = [];
        include(self::PATH_TO_LANG.'/'.$lang.'.lang.php');
        if (isset($aqText['AQ_'.$aqIndex])) {
            return $aqText['AQ_'.$aqIndex];
        }

        return '';
    }


}