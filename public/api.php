<?php

declare(strict_types=1);

use GuzzleHttp\Psr7\ServerRequest;
use Sportrizer\Sportysky\ApiClient;
use Sportrizer\Sportysky\Authenticator;
use Sportrizer\Sportysky\ServerRequestHandler;
use Laminas\HttpHandlerRunner\Emitter\SapiStreamEmitter;
use GuzzleHttp\HandlerStack;
use Kevinrob\GuzzleCache\CacheMiddleware;
use Kevinrob\GuzzleCache\Strategy\PublicCacheStrategy;
use Doctrine\Common\Cache\FilesystemCache;
use Kevinrob\GuzzleCache\Storage\DoctrineCacheStorage;

require '../vendor/autoload.php';

// Authenticate the server to SportRIZER
$authenticator = new Authenticator(getenv('SPORTYSKY_CLIENT_ID'), getenv('SPORTYSKY_CLIENT_SECRET'));

// CacheHandler - FileSystem
$cacheHandler = HandlerStack::create();
$cacheHandler->push(
    new CacheMiddleware(
        new PublicCacheStrategy(
            new DoctrineCacheStorage(
                new FilesystemCache('/tmp/demomap')
            )
        )
    )
);
// Create a new SportySKY API client
// with the JWT token provided by the authenticator
$apiClient = new ApiClient($authenticator->getToken(), $cacheHandler);

// Handles the request made by the JS API
$apiResponse = (new ServerRequestHandler($apiClient))->handle(ServerRequest::fromGlobals());

// Outputs the SportySKY API response
(new SapiStreamEmitter())->emit($apiResponse);