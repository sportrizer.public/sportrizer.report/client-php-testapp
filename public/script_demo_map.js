window.addEventListener("DOMContentLoaded", function () {

    var clearActiveHeures = function () {
        for (var j = 0; j < document.getElementsByClassName('bloc_heures activ').length; j++) {
            document.getElementsByClassName('bloc_heures activ')[j].classList.remove('activ');
        }
    };

    var launchCurrentTime = function () {
        var evt = new MouseEvent('click', {
            bubbles: true,
            cancelable: true,
            view: window
        });
        // If cancelled, don't dispatch our event
        var canceled = !document.getElementsByClassName('bloc_heures activ')[0].dispatchEvent(evt);
    };

    var onHoverSpot = function (spot, event) {
        switch (event.type) {
            case 'mousemove':
                switch (currentType) {
                    case 'country':
                        document.getElementById('text_follow').innerText = spot.region.name;
                        break;
                    case 'region':
                        document.getElementById('text_follow').innerText = spot.department.name;
                        break;
                    case 'department':
                        document.getElementById('text_follow').innerText = spot.title;
                        break;
                }
                document.getElementById('text_follow').style.display = 'block';
                document.getElementById('text_follow').style.left = event.pageX - 15 + "px";
                document.getElementById('text_follow').style.top = event.pageY - 30 + "px";
                break;
            case 'mouseleave':
                document.getElementById('text_follow').style.display = 'none';
                break
        }
    };

    var onClickSpot = function (spot, event) {
        switch (currentType) {
            case 'country':
                window.location.href = '/region.php?isocode=' + spot.region.isoCode;
                break;
            case 'region':
                window.location.href = '/department.php?isocode=' + spot.department.isoCode;
                break;
            case 'department':
                window.location.href = '/details.php?spot=' + spot.id;
                break;
        }
    };


    var hasAqData = function (hasData) {
        if (!hasData) {
            document.getElementById('no_aq').style.display = 'none';
        } else {
            document.getElementById('no_aq').style.display = 'block';
        }
    };

    var map = new sportrizer.sportysky.Map(
        document.getElementById('map'),
        'api.php', // or api_redis.php
        {
            iconSize: 40,
            iconSizeTiny: 20,
            iconTinyBefore: 767,
            debug: false,
            textWeather: '[AIRTEMPERATURE] °c',
            textWind: '[WINDSPEED] Km/h',
            textWindGust: '<br><em>(Max. [WINDGUST])</em>',
            hoverSpot: onHoverSpot,
            clickSpot: onClickSpot,
            hasAqData: hasAqData,
        });

    for (var i = 0; i < document.getElementsByClassName('bloc_heures').length; i++) {
        document.getElementsByClassName('bloc_heures')[i].addEventListener('click', function (event) {
            clearActiveHeures();
            event.currentTarget.classList.add('activ');
            var elDay = document.getElementsByClassName('bloc_day activ')[0];
            var date = new Date();
            date.setTime(elDay.getAttribute('data-time'));
            switch (event.currentTarget.id) {
                case 'date_morning':
                    date.setHours(8);
                    break;
                case 'date_midday':
                    date.setHours(12);
                    break;
                case 'date_evening':
                    date.setHours(18);
                    break;
                case 'date_night':
                    date.setHours(22);
                    break;
            }
            switch (currentType) {
                case 'country':
                    map.displayCountry(date, currentIso);
                    break;
                case 'region':
                    map.displayRegion(date, currentIso);
                    break;
                case 'department':
                    map.displayDepartment(date, currentIso);
                    break;
            }
        });
    }

    for (var i = 0; i < document.getElementsByClassName('bloc_day').length; i++) {
        document.getElementsByClassName('bloc_day')[i].addEventListener('click', function (event) {
            var currentDayMidnight = new Date();
            currentDayMidnight.setHours(0);
            currentDayMidnight.setMinutes(0);
            currentDayMidnight.setSeconds(0);
            var currentDayNight = new Date();
            currentDayNight.setHours(23);
            currentDayNight.setMinutes(59);
            currentDayNight.setSeconds(59);
            var current = new Date();
            var dateClick = new Date();
            dateClick.setTime(this.getAttribute('data-time') * 1000);
            document.getElementsByClassName('bloc_day activ')[0].classList.remove('activ');
            event.currentTarget.classList.add('activ');
            clearActiveHeures();
            if (dateClick.getTime() > currentDayNight.getTime()) {
                document.getElementById('date_morning').classList.add('activ');
            } else {
                if ((current.getHours() >= 0) && (current.getHours() < 10)) {
                    document.getElementById('date_morning').classList.add('activ');
                }
                if ((current.getHours() >= 10) && (current.getHours() < 16)) {
                    document.getElementById('date_midday').classList.add('activ');
                }
                if ((current.getHours() >= 16) && (current.getHours() < 22)) {
                    document.getElementById('date_evening').classList.add('activ');
                }
                if ((current.getHours() >= 22) || (current.getHours() < 0)) {
                    document.getElementById('date_night').classList.add('activ');
                }
            }
            launchCurrentTime();

            var elX = [].indexOf.call(event.currentTarget.parentNode.children, event.currentTarget);
            var width = event.currentTarget.offsetWidth;
            var style = getComputedStyle(event.currentTarget);
            width += parseInt(style.marginLeft) + parseInt(style.marginRight);
            document.getElementById('heures_list').style.opacity = '0';
            event.currentTarget.parentNode.style.marginLeft = (-1 * (elX * width)) + "px";
            setTimeout(function () {
                document.getElementById('heures_list').style.opacity = '1';
            }, 700);
        });

        document.getElementById('previous_date').addEventListener('click', function (event) {
            var currentActive = document.getElementsByClassName('bloc_day activ')[0];
            var elX = [].indexOf.call(currentActive.parentNode.children, currentActive);
            if (elX >= 1) {
                var evt = new MouseEvent('click', {
                    bubbles: true,
                    cancelable: true,
                    view: window
                });
                var canceled = !currentActive.parentNode.children[elX - 1].dispatchEvent(evt);
            }
            event.stopImmediatePropagation();
        });
        document.getElementById('next_date').addEventListener('click', function (event) {
            var currentActive = document.getElementsByClassName('bloc_day activ')[0];
            var elX = [].indexOf.call(currentActive.parentNode.children, currentActive);
            if (elX < currentActive.parentNode.children.length) {
                var evt = new MouseEvent('click', {
                    bubbles: true,
                    cancelable: true,
                    view: window
                });
                var canceled = !currentActive.parentNode.children[elX + 1].dispatchEvent(evt);
            }
            event.stopImmediatePropagation();
        });
    }
    window.onresize = function () {
        map.redraw();
    };

    document.getElementById('show-aq').onclick = function () {
        map.showAQ();
    };

    document.getElementById('show-wind').onclick = function () {
        map.showWind();
    };

    document.getElementById('show-weather').onclick = function () {
        map.showWeather();
    };

    launchCurrentTime();
});