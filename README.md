# SportySKY API - Demo Package


## Install

``` shell
$ git clone git@gitlab.com:sportrizer.public/sportrizer.report/client-php-testapp.git
$ cd sportysky-client-php-testapp
$ composer install
```

## Requirements

 - PHP ^7.2
 - [Composer](https://getcomposer.org/)
 - ClientID & ClientSecret **API Sportysky** 

## Ask demo

You can asking demo link at [contact@sportrizer.com](contact@sportrizer.com)

## Getting started

#### Vhost for Apache / Nginx

**Apache**
```
SetEnv SPORTYSKY_CLIENT_ID YOURCLIENDIT
SetEnv SPORTYSKY_CLIENT_SECRET YOURCLIENTSECRET
``` 

**Nginx to Php-FPM**
``` 
fastcgi_param SPORTYSKY_CLIENT_ID YOURCLIENDIT;
fastcgi_param SPORTYSKY_CLIENT_SECRET YOURCLIENTSECRET;
``` 

#### Configuration
If you can't modify Vhost, you can modify directly _api.php_ line 14 as : 
``` php
$authenticator = new Authenticator('YOURCLIENDIT', 'YOURCLIENTSECRET');
``` 

## Standalone Integration

* [PHP client](https://github.com/SportRIZER/sportysky-client-php#integration-with-the-sportysky-javascript-library)
* [JS Lib](https://bitbucket.org/sportrizer/sportysky-js-lib)
